/*
 * container_of.h
 *
 *  Created on: 3 мар. 2021 г.
 *      Author: bekir
 */

#ifndef UTILS_INC_CONTAINER_OF_H_
#define UTILS_INC_CONTAINER_OF_H_

#include <stddef.h>


#define container_of(ptr, type, member) ({					\
	const typeof( ((type *)0)->member ) *__mptr = (ptr);	\
	(type *)( (char *)__mptr - offsetof(type,member) );})


#endif /* UTILS_INC_CONTAINER_OF_H_ */
