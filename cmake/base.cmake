# include(${PROJECT_SOURCE_DIR}/cmake/color_msg.cmake)

function(LibBuild target_name include_dirs)
	file(GLOB SOURCES_LIB *.c Src/*.c)
	file(GLOB SOURCES_LIB_ASM *.S Src/*.c)
 	set_property(SOURCE ${SOURCES_LIB_ASM} PROPERTY LANGUAGE C)
	# message(STATUS "SOURSES_LIB_${target_name}: ${SOURCES_LIB} ${SOURCES_LIB_ASM}")
	color_message(STATUS Green "SOURSES_LIB_${target_name}:")
	color_message(STATUS ColorReset "${SOURCES_LIB} ${SOURCES_LIB_ASM}")
	add_library(${target_name} STATIC ${SOURCES_LIB} ${SOURCES_LIB_ASM})
	add_library(${TARGET}::${target_name} ALIAS ${target_name})
	
    math(EXPR lastIndex "${ARGC}-1")
	foreach(index RANGE 1 ${lastIndex})
		target_include_directories(${target_name} PUBLIC ${ARGV${index}})
		# message(STATUS "INCLUDE_DIRS_LIB_${target_name}: ${ARGV${index}}")
		color_message(STATUS Green "INCLUDE_DIRS_LIB_${target_name}:")
		color_message(STATUS ColorReset "${ARGV${index}}")
	endforeach(index RANGE 1 ${lastIndex})	
endfunction(LibBuild)

function(BinFileGenerate target_name)
	add_custom_command(TARGET ${target_name}.elf
	POST_BUILD COMMAND ${CMAKE_OBJCOPY} -O binary 
	-j .isr_vector 
	-j .size_section
	-j .text 
	-j .rodata
	-j ARM.extab
	-j .ARM
	-j .preinit_array -j .init_array -j .fini_array
	-j .data
	-S ${target_name}.elf ${target_name}.bin
	COMMAND ${PROJECT_SOURCE_DIR}/scripts/crc32_to_file.sh ${target_name}.bin
	COMMAND  ${CMAKE_COMMAND} -E rename ${target_name}.bin ${target_name}_${CURRENT_TIMESTAMP}.bin
	COMMENT "Generate bin file")
	add_custom_target(${target_name}_${CURRENT_TIMESTAMP}.bin DEPENDS ${target_name}.elf)
	set_target_properties(
	${target_name}_${CURRENT_TIMESTAMP}.bin
	PROPERTIES ADDITIONAL_CLEAN_FILES "${CMAKE_CURRENT_BINARY_DIR}/${target_name}_${CURRENT_TIMESTAMP}.bin"
	)
endfunction(BinFileGenerate)

function(HexFileGenerate target_name)
	add_custom_command(TARGET ${target_name}.elf
	POST_BUILD COMMAND ${CMAKE_OBJCOPY} -O ihex ${target_name}.elf ${target_name}.hex
	COMMAND  ${CMAKE_COMMAND} -E rename ${target_name}.hex ${target_name}_${CURRENT_TIMESTAMP}.hex
	COMMENT "Generate hex file")
	add_custom_target(${target_name}_${CURRENT_TIMESTAMP}.hex DEPENDS ${target_name}.elf)
	set_target_properties(${target_name}_${CURRENT_TIMESTAMP}.hex 
	PROPERTIES ADDITIONAL_CLEAN_FILES "${CMAKE_CURRENT_BINARY_DIR}/${target_name}.hex"
	)
endfunction(HexFileGenerate)

function(ListFileGenerate target_name)
	add_custom_command(TARGET ${target_name}.elf
	POST_BUILD COMMAND ${CMAKE_OBJDUMP} -Sdh ${target_name}.elf > ${target_name}.lst
	COMMENT "Generate listing file")
endfunction(ListFileGenerate)

function(FlashingCmd target_name path_openocd)
	add_custom_command(TARGET ${target_name}_${CURRENT_TIMESTAMP}.hex
	POST_BUILD COMMAND ${PROJECT_SOURCE_DIR}/scripts/flashing_script ${path_openocd} 
	${PROJECT_SOURCE_DIR}/scripts/openocd_stlink_v2.cfg ${CMAKE_CURRENT_BINARY_DIR}/${target_name}_${CURRENT_TIMESTAMP}.hex
	COMMENT "Flashing MCU")
	add_custom_target(flash_mcu DEPENDS ${target_name}_${CURRENT_TIMESTAMP}.hex)
endfunction(FlashingCmd)

function(TargetInstall target_name)
	if(TARGET ${target_name.elf})
	install(FILES 
	${CMAKE_BINARY_DIR}/${target_name}.elf 
	${CMAKE_BINARY_DIR}/${target_name}_${CURRENT_TIMESTAMP}.bin 
	${CMAKE_BINARY_DIR}/${target_name}_${CURRENT_TIMESTAMP}.hex
	DESTINATION ${PROJECT_SOURCE_DIR}/install
	CONFIGURATIONS Debug Release)
	endif(TARGET ${target_name.elf})
endfunction(TargetInstall target_names)
