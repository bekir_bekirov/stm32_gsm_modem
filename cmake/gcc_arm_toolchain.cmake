set(CMAKE_SYSTEM_NAME Generic)
set(CMAKE_SYSTEM_PROCESSOR arm)

############################################### USER DEFINITION #########################################################
set(TOOLCHAIN_PREFIX "/home/bekir/STM32_Toolchain/gcc-arm-none-eabi-9-2020-q2-update" CACHE STRING "Path to toolchain")
set(OPENOCD_PATH "/home/bekir/STM32_Toolchain/openocd/0.10.0-dev/bin/openocd" CACHE STRING "Path to OpenOCD")
################################################ END USER DEFINITION ####################################################

set(CMAKE_TRY_COMPILE_TARGET_TYPE STATIC_LIBRARY)

set (CMAKE_FIND_LIBRARY_PREFIXES lib)
set (CMAKE_FIND_LIBRARY_SUFFIXES .a)

set (CMAKE_ASM_COMPILER "${TOOLCHAIN_PREFIX}/bin/arm-none-eabi-gcc")
set (CMAKE_C_COMPILER "${TOOLCHAIN_PREFIX}/bin/arm-none-eabi-gcc")
set (CMAKE_CXX_COMPILER "${TOOLCHAIN_PREFIX}/bin/arm-none-eabi-g++")
set (CMAKE_OBJCOPY "${TOOLCHAIN_PREFIX}/bin/arm-none-eabi-objcopy")
set (CMAKE_OBJDUMP "${TOOLCHAIN_PREFIX}/bin/arm-none-eabi-objdump")
set (CMAKE_SIZE "${TOOLCHAIN_PREFIX}/bin/arm-none-eabi-size")

#set (TOOLCHAIN_INC_DIR "${TOOLCHAIN_PREFIX}/arm-none-eabi/include")
#set (TOOLCHAIN_LIB_DIR "${TOOLCHAIN_PREFIX}/arm-none-eabi/lib")
#set (TOOLCHAIN_BIN_DIR "${TOOLCHAIN_PREFIX}/bin")

set(CMAKE_C_COMPILER_WORKS TRUE)
set(CMAKE_CXX_COMPILER_WORKS TRUE)
