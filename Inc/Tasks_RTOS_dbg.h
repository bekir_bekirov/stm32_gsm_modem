#ifndef INC_TASKS_RTOS_DBG_H
#define INC_TASKS_RTOS_DBG_H


//------------------------------------------------------------------- Test debug -------------------------------------------------------------
#if (DEBUG_LEVEL > 2)

#ifdef DEBUG_VERBOSE
#define LOGI	LOGV_INFO
#else
#define LOGI	LOG_INFO
#endif

#else
#define LOGI(...)
#endif

#if (DEBUG_LEVEL > 3)

#ifdef DEBUG_VERBOSE
#define LOGD	LOGV_DBG
#else
#define LOGD	LOG_DBG
#endif

#else
#define LOGD(...)
#endif

#if (DEBUG_LEVEL > 0)

#ifdef DEBUG_VERBOSE
#define LOGE	LOGV_ERROR
#else
#define LOGE	LOG_ERROR
#endif

#else
#define LOGE(...)
#endif

#if (DEBUG_LEVEL > 1)

#ifdef DEBUG_VERBOSE
#define LOGW	LOGV_WARN
#else
#define LOGW	LOG_WARN
#endif

#else
#define LOGW(...)
#endif
//-----------------------------------------------------------------------------------------------------------------------------------------------

#endif