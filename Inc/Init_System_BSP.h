/*
 * Init_System_BSP.h
 *
 *  Created on: 5 мар. 2021 г.
 *      Author: bekir
 */

#ifndef INC_INIT_SYSTEM_BSP_H_
#define INC_INIT_SYSTEM_BSP_H_

#include "stm32f4xx.h"

#include <stdatomic.h>
#include "uart.h"
#include "dbg.h"

#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "semphr.h"

#include "dbg.h"


// void SystemClock_Config(void);
// void BSP_Leds_Init(void);
// void BSP_Led3_On(void);
// void BSP_Led3_Off(void);
// void BSP_Led3_Toggle(void);
// void BSP_Led4_On(void);
// void BSP_Led4_Off(void);
// void BSP_Led4_Toggle(void);

static inline void BSP_Led3_On(void)
{
  HAL_GPIO_WritePin(GPIOG, GPIO_PIN_13, GPIO_PIN_SET);
}

static inline void BSP_Led3_Off(void)
{
  HAL_GPIO_WritePin(GPIOG, GPIO_PIN_13, GPIO_PIN_RESET);
}

static inline void BSP_Led3_Toggle(void)
{
  HAL_GPIO_TogglePin(GPIOG, GPIO_PIN_13);
}

static inline void BSP_Led4_On(void)
{
  HAL_GPIO_WritePin(GPIOG, GPIO_PIN_14, GPIO_PIN_SET);
}

static inline void BSP_Led4_Off(void)
{
  HAL_GPIO_WritePin(GPIOG, GPIO_PIN_14, GPIO_PIN_RESET);
}

static inline void BSP_Led4_Toggle(void)
{
  HAL_GPIO_TogglePin(GPIOG, GPIO_PIN_14);
}

#endif /* INC_INIT_SYSTEM_BSP_H_ */
