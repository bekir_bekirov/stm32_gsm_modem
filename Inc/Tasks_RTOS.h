/*
 * Tasks_RTOS.h
 *
 *  Created on: 5 мар. 2021 г.
 *      Author: bekir
 */

#ifndef INC_TASKS_RTOS_H_
#define INC_TASKS_RTOS_H_

#include "stm32f4xx.h"

#include <stdatomic.h>
#include "uart.h"
#include "dbg.h"

#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "semphr.h"

#include "dbg.h"
#include "Init_System_BSP.h"


void TestTask1(void* pvParameters);
void TestTask2(void* pvParameters);
void AtCommTask(void* pvParameters);

#endif /* INC_TASKS_RTOS_H_ */
