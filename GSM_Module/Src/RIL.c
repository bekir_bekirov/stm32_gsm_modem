#include "dbg.h"

#define DEBUG_LEVEL 4
#define DEBUG_VERBOSE
#include "at_dbg.h"

#include "RIL.h"

void requestSignalStrength(UART_Driver_t *uart_drv_handle, void *data, size_t datalen)
{
    ATResponse *p_response = NULL;
    int err;
    char *line, *line_buf;
    int count = 0;
    // Accept a response that is at least v6, and up to v10
    int minNumOfElements=sizeof(RIL_SignalStrength_v6)/sizeof(int);
    int maxNumOfElements=sizeof(RIL_SignalStrength_v10)/sizeof(int);
    int response[maxNumOfElements];

    memset(response, 0, sizeof(response));

    err = at_send_command_singleline(uart_drv_handle, "AT+CSQ\r", "+CSQ:", &p_response);

    if (err < 0 || p_response->success == 0) {
        // RIL_onRequestComplete(t, RIL_E_GENERIC_FAILURE, NULL, 0);
        goto error;
    }

    line = p_response->p_intermediates->line;
    line_buf = line;

    err = at_tok_start(&line);
    if (err < 0)
    	goto error;

    for (count = 0; count < maxNumOfElements; count++) {
        err = at_tok_nextint(&line, &(response[count]));
        if (err < 0 && count < 1)
        	goto error;
    }

    LOGD(ril, "requestSignalStrength line is %s\r\n", line_buf);

    // RIL_onRequestComplete(t, RIL_E_SUCCESS, response, sizeof(response));

    at_response_free(p_response);
    return;

error:
    LOGE(ril, "requestSignalStrength must never return an error when radio is on\r\n");
    // RIL_onRequestComplete(t, RIL_E_GENERIC_FAILURE, NULL, 0);
    at_response_free(p_response);
}
