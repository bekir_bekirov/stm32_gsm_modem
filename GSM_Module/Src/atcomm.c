#include "dbg.h"

#define DEBUG_LEVEL 4
#define DEBUG_VERBOSE
// #define AT_DEBUG
#include "at_dbg.h"
#include "atcomm.h"
#include "atcomm_cgf.h"
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "misc.h"

//ring buffer for rx symbols
// static UT_ringbuffer *at_rx_ring_buf_ptr;
// UT_icd ut_char_icd = {sizeof(uint8_t), NULL, NULL, NULL };

static QueueHandle_t at_rx_queue;
static AT_COM_RTOS_MUTEX_TYPE at_command_mutex;
AT_COM_RTOS_MUTEX_TYPE at_comm_semphr;

static void DefaultUnsolictedHandler(const char *s, const char *sms_pdu);

// at commands buffer
static char s_ATBuffer[MAX_AT_RESPONSE + 1];
static char *s_ATBufferCur = s_ATBuffer;
static ATUnsolHandler s_unsolHandler = DefaultUnsolictedHandler;
static ATCommandType s_type;
static const char *s_responsePrefix = NULL;
static const char *s_smsPDU = NULL;
static ATResponse *sp_response = NULL;
static int s_readerClosed;

#ifdef AT_DEBUG
void  AT_DUMP(const char *buff, int len)
{
    if (len < 0)
        len = strlen(buff);
    LOGD(modem, "<< %u, %s\r\n", len, buff);
}
#else
#define AT_DUMP(...)
#endif

/** add an intermediate response to sp_response*/
static void addIntermediate(const char *line)
{
    ATLine *p_new;

    p_new = (ATLine *) malloc(sizeof(ATLine));

    p_new->line = strdup(line);

    /* note: this adds to the head of the list, so the list
       will be in reverse order of lines received. the order is flipped
       again before passing on to the command issuer */
    p_new->p_next = sp_response->p_intermediates;
    sp_response->p_intermediates = p_new;
}

static ATResponse *at_response_new()
{
    return (ATResponse *) calloc(1, sizeof(ATResponse));
}

void at_response_free(ATResponse *p_response)
{
    ATLine *p_line;

    if (p_response == NULL) return;

    p_line = p_response->p_intermediates;

    while (p_line != NULL) {
        ATLine *p_toFree;

        p_toFree = p_line;
        p_line = p_line->p_next;

        free(p_toFree->line);
        free(p_toFree);
    }

    free (p_response->finalResponse);
    free (p_response);
}

static const char * s_finalResponsesSuccess[] = {
    "OK",
    "CONNECT"       /* some stacks start up data on another channel */
};
static int isFinalResponseSuccess(const char *line)
{
    size_t i;

    for (i = 0 ; i < NUM_ELEMS_IN_TABLE(s_finalResponsesSuccess) ; i++) {
        if (strStartsWith(line, s_finalResponsesSuccess[i])) {
            return 1;
        }
    }

    return 0;
}

static void DefaultUnsolictedHandler(const char *s, const char *sms_pdu)
{
    LOGD(modem, "UnsolictedHandler is invoked\r\n");

    return;
}

/** assumes s_commandmutex is held */
static void handleFinalResponse(const char *line)
{
    sp_response->finalResponse = strdup(line);
    LOGD(modem, "Final response, %s\r\n", sp_response->finalResponse);

    // pthread_cond_signal(&s_commandcond);
}

static int writeCtrlZ (const char *s)
{
    size_t cur = 0;
    size_t len = strlen(s);
    size_t written;

    if (s_readerClosed > 0) {
        return AT_ERROR_CHANNEL_CLOSED;
    }

    LOGD(modem, "AT> %s^Z\n", s);
    AT_DUMP( ">* ", s, strlen(s) );

    /* the main string */
    while (cur < len) {
        // do {
        //     written = write (s_fd, s + cur, len - cur);
        // } while (written < 0 && errno == EINTR);
        if (written < 0) {
            return AT_ERROR_GENERIC;
        }
        cur += written;
    }

    /* the ^Z  */

    // do {
    //     written = write (s_fd, "\032" , 1);
    // } while ((written < 0 && errno == EINTR) || (written == 0));

    if (written < 0) {
        return AT_ERROR_GENERIC;
    }

    return 0;
}

static void clearPendingCommand()
{
    if (sp_response != NULL) {
        at_response_free(sp_response);
    }

    sp_response = NULL;
    s_responsePrefix = NULL;
    s_smsPDU = NULL;
}

void InitATChannel(void)
{
    // utringbuffer_new(at_rx_ring_buf_ptr, (MAX_AT_RESPONSE + 1), &ut_char_icd);
    at_rx_queue = xQueueCreate(MAX_AT_RESPONSE + 1, sizeof(uint8_t));
    at_command_mutex = AT_COM_RTOS_MUTEX_CREATE();
    at_comm_semphr = AT_COM_RTOS_SEMPHR_CREATE();
    // sp_response = (ATResponse *)calloc(1, sizeof(ATResponse));
}

/**
 * returns 1 if line is a final response indicating error
 * See 27.007 annex B
 * WARNING: NO CARRIER and others are sometimes unsolicited
 */
static const char * s_finalResponsesError[] = {
    "ERROR",
    "+CMS ERROR:",
    "+CME ERROR:",
    "NO CARRIER", /* sometimes! */
    "NO ANSWER",
    "NO DIALTONE",
};
static int isFinalResponseError(const char *line)
{
    size_t i;

    for (i = 0 ; i < NUM_ELEMS_IN_TABLE(s_finalResponsesError) ; i++) {
        if (strStartsWith(line, s_finalResponsesError[i])) {
            return 1;
        }
    }

    return 0;
}

/**
 * returns 1 if line is the first line in (what will be) a two-line
 * SMS unsolicited response
 */
static const char * s_smsUnsoliciteds[] = {
    "+CMT:",
    "+CDS:",
    "+CBM:"
};
static int isSMSUnsolicited(const char *line)
{
    size_t i;

    for (i = 0 ; i < NUM_ELEMS_IN_TABLE(s_smsUnsoliciteds) ; i++) {
        if (strStartsWith(line, s_smsUnsoliciteds[i])) {
            return 1;
        }
    }

    return 0;
}

static void handleUnsolicited(const char *line)
{
    if (s_unsolHandler != NULL) {
        s_unsolHandler(line, NULL);
    }
}

/**
 * The line reader places the intermediate responses in reverse order
 * here we flip them back
 */
static void reverseIntermediates(ATResponse *p_response)
{
    ATLine *pcur,*pnext;

    pcur = p_response->p_intermediates;
    p_response->p_intermediates = NULL;

    while (pcur != NULL) {
        pnext = pcur->p_next;
        pcur->p_next = p_response->p_intermediates;
        p_response->p_intermediates = pcur;
        pcur = pnext;
    }
}

void DeInitATChannel(void)
{
    // utringbuffer_free(at_rx_ring_buf_ptr);
    at_response_free(sp_response);
}

void AT_RX_IQR_Handler(UART_HandleTypeDef *huart)
{
    uint8_t at_rx_symbol;

    UART_Driver_t *uart_ctx = container_of(huart, UART_Driver_t, uart_instance);
    at_rx_symbol = UART_Rx_Get_Byte(uart_ctx);
    // utringbuffer_push_back(at_rx_ring_buf_ptr, &at_rx_symbol);
    xQueueSendToBackFromISR(at_rx_queue, &at_rx_symbol, NULL);

    HAL_GPIO_TogglePin(GPIOG, GPIO_PIN_14);
}

/**
 * Returns a pointer to the end of the next line
 * special-cases the "> " SMS prompt
 *
 * returns NULL if there is no complete line
 */
static char * findNextEOL(char *cur)
{
    if (cur[0] == '>' && cur[1] == ' ' && cur[2] == '\0') {
        /* SMS prompt character...not \r terminated */
        return cur+2;
    }
    // Find next newline
    while (*cur != '\0' && *cur != '\r' && *cur != '\n') cur++;

    return *cur == '\0' ? NULL : cur;
}

static void processLine(const char *line)
{
    // pthread_mutex_lock(&s_commandmutex);

    if (sp_response == NULL) {
        /* no command pending */
        handleUnsolicited(line);
    } else if (isFinalResponseSuccess(line)) {
        sp_response->success = 1;
        handleFinalResponse(line);
    } else if (isFinalResponseError(line)) {
        sp_response->success = 0;
        handleFinalResponse(line);
    } else if (s_smsPDU != NULL && 0 == strcmp(line, "> ")) {
        // See eg. TS 27.005 4.3
        // Commands like AT+CMGS have a "> " prompt
        writeCtrlZ(s_smsPDU);
        s_smsPDU = NULL;
    } else switch (s_type) {
        case NO_RESULT:
            handleUnsolicited(line);
            break;
        case NUMERIC:
            if (sp_response->p_intermediates == NULL
                && isdigit(line[0])
            ) {
                addIntermediate(line);
            } else {
                /* either we already have an intermediate response or
                   the line doesn't begin with a digit */
                handleUnsolicited(line);
            }
            break;
        case SINGLELINE:
            if (sp_response->p_intermediates == NULL
                && strStartsWith (line, s_responsePrefix)
            ) {
                addIntermediate(line);
            } else {
                /* we already have an intermediate response */
                handleUnsolicited(line);
            }
            break;
        case MULTILINE:
            if (strStartsWith (line, s_responsePrefix)) {
                addIntermediate(line);
            } else {
                handleUnsolicited(line);
            }
        break;

        default: /* this should never be reached */
            LOGE(modem, "Unsupported AT command type %d\n", s_type);
            handleUnsolicited(line);
        break;
    }

    // pthread_mutex_unlock(&s_commandmutex);
}

const char *readline()
{
    char *p_read = NULL;
    char *p_eol = NULL;
    char *ret;
    uint16_t i = 0;

    /* this is a little odd. I use *s_ATBufferCur == 0 to
     * mean "buffer consumed completely". If it points to a character, than
     * the buffer continues until a \0
     */
    if (*s_ATBufferCur == '\0') {
        /* empty buffer */
        s_ATBufferCur = s_ATBuffer;
        *s_ATBufferCur = '\0';
        p_read = s_ATBuffer;
    } else {   /* *s_ATBufferCur != '\0' */
        /* there's data in the buffer from the last read */

        // skip over leading newlines
        while (*s_ATBufferCur == '\r' || *s_ATBufferCur == '\n')
            s_ATBufferCur++;

        p_eol = findNextEOL(s_ATBufferCur);

        if (p_eol == NULL) {
            /* a partial line. move it up and prepare to read more */
            size_t len;
            len = strlen(s_ATBufferCur);
            memmove(s_ATBuffer, s_ATBufferCur, len + 1);
            p_read = s_ATBuffer + len;
            s_ATBufferCur = s_ATBuffer;
        }
        /* Otherwise, (p_eol !- NULL) there is a complete line  */
        /* that will be returned the while () loop below        */
    }

    while (p_eol == NULL) {
        if (0 == MAX_AT_RESPONSE - (p_read - s_ATBuffer)) {
            LOGE(modem, "Input line exceeded buffer\r\n");
            /* ditch buffer and start over again */
            s_ATBufferCur = s_ATBuffer;
            *s_ATBufferCur = '\0';
            p_read = s_ATBuffer;
        }

        // do {
        //     count = read(s_fd, p_read,
        //                     MAX_AT_RESPONSE - (p_read - s_ATBuffer));
        // } while (count < 0 && errno == EINTR);

        size_t count = 0;
        do {
            while (xQueueReceive(at_rx_queue, p_read + i, 50) != pdFALSE) {
                ++i;
                ++count;
            }
        } while (count <= 0);

        if (count > 0) {
            AT_DUMP(p_read, count);
            p_read[count] = '\0';
            // skip over leading newlines
            while (*s_ATBufferCur == '\r' || *s_ATBufferCur == '\n')
                s_ATBufferCur++;
            p_eol = findNextEOL(s_ATBufferCur);
            p_read += count;
        } else {
            if (count <= 0) {
                /* read error encountered or EOF reached */
                if(count == 0) {
                    LOGD(modem, "atchannel: EOF reached\r\n");
                } else {
                    LOGD(modem, "atchannel: read error %s\r\n", "-1");
                }
            }
            return NULL;
        }
    }

    /* a full line in the buffer. Place a \0 over the \r and return */
    ret = s_ATBufferCur;
    *p_eol = '\0';
    s_ATBufferCur = p_eol + 1; /* this will always be <= p_read,    */
                              /* and there will be a \0 at *p_read */
    LOGD(modem, "AT< %s\r\n", ret);
    // AT_COM_RTOS_SEMPHR_GIVE(at_comm_semphr);

    return ret;
}

void *readerLoop(void)
{
    for (;;) {
        const char * line;

        line = readline();
        LOGD(modem, "line is %s\r\n", line);

        if (line == NULL) {
            break;
        }

        if(isSMSUnsolicited(line)) {
            char *line1;
            const char *line2;

            // The scope of string returned by 'readline()' is valid only
            // till next call to 'readline()' hence making a copy of line
            // before calling readline again.
            line1 = strdup(line);
            line2 = readline();

            if (line2 == NULL) {
                free(line1);
                break;
            }

            if (s_unsolHandler != NULL) {
                s_unsolHandler (line1, line2);
            }
            free(line1);
        }
        else {
            processLine(line);
        }
        AT_COM_RTOS_SEMPHR_GIVE(at_comm_semphr);
        HAL_GPIO_TogglePin(GPIOG, GPIO_PIN_13);
    }

    return NULL;
}

int32_t at_send_command_full(UART_Driver_t *uart_drv_handle, const char *command, ATCommandType type,
                    const char *responsePrefix, const char *smspdu, ATResponse **pp_outResponse, TickType_t timeout)
{
    int32_t res;

    if (AT_COM_RTOS_MUTEX_LOCK(at_command_mutex, timeout) == pdFALSE) return -1;

    s_type = type;
    s_responsePrefix = responsePrefix;
    s_smsPDU = smspdu;
    sp_response = at_response_new();

    if (AT_COM_RTOS_SEMPHR_GET_CNT(at_comm_semphr)) {
        if (AT_COM_RTOS_SEMPHR_TAKE(at_comm_semphr, timeout) == pdFALSE) {
            at_response_free(sp_response);
            sp_response = NULL;
            AT_COM_RTOS_MUTEX_UNLOCK(at_command_mutex);
            return -1;
        }
    }

    if (UART_Tx(uart_drv_handle, command, strlen(command), timeout) != 0) {
        at_response_free(sp_response);
        sp_response = NULL;
        AT_COM_RTOS_MUTEX_UNLOCK(at_command_mutex);
        return -1;
    }
    if (AT_COM_RTOS_SEMPHR_TAKE(at_comm_semphr, timeout) == pdFALSE) {
        at_response_free(sp_response);
        sp_response = NULL;
        AT_COM_RTOS_MUTEX_UNLOCK(at_command_mutex);
        return -1;
    }

     if (pp_outResponse == NULL) {
         at_response_free(sp_response);
     } else {
         /* line reader stores intermediate responses in reverse order */
         reverseIntermediates(sp_response);
         *pp_outResponse = sp_response;
         (*pp_outResponse)->success = 1;
     }
    sp_response = NULL;

    AT_COM_RTOS_MUTEX_UNLOCK(at_command_mutex);

    return 0;
}

/**
 * Issue a single normal AT command with no intermediate response expected
 *
 * "command" should not include \r
 * pp_outResponse can be NULL
 *
 * if non-NULL, the resulting ATResponse * must be eventually freed with
 * at_response_free
 */
int32_t at_send_command(UART_Driver_t *uart_drv_handle, const char *command, ATResponse **pp_outResponse)
{
    int32_t err;

    err = at_send_command_full(uart_drv_handle, command, NO_RESULT, NULL, NULL, pp_outResponse, AT_COMM_DEFAULT_TIMEOUT);

    return err;
}

int32_t at_send_command_singleline(UART_Driver_t *uart_drv_handle, const char *command, const char *responsePrefix,
                                    ATResponse **pp_outResponse)
{
    int32_t err;

    err = at_send_command_full(uart_drv_handle, command, SINGLELINE, responsePrefix,
                                NULL, pp_outResponse, AT_COMM_DEFAULT_TIMEOUT);

    if (err == 0 && pp_outResponse != NULL
        && (*pp_outResponse)->success > 0
        && (*pp_outResponse)->p_intermediates == NULL
    ) {
        /* successful command must have an intermediate response */
        at_response_free(*pp_outResponse);
        *pp_outResponse = NULL;
        return AT_ERROR_INVALID_RESPONSE;
    }

    return err;
}

int32_t at_send_command_numeric(UART_Driver_t *uart_drv_handle, const char *command, ATResponse **pp_outResponse)
{
    int32_t err;

    err = at_send_command_full(uart_drv_handle, command, NUMERIC, NULL, NULL, pp_outResponse, AT_COMM_DEFAULT_TIMEOUT);

    if (err == 0 && pp_outResponse != NULL
        && (*pp_outResponse)->success > 0
        && (*pp_outResponse)->p_intermediates == NULL
    ) {
        /* successful command must have an intermediate response */
        at_response_free(*pp_outResponse);
        *pp_outResponse = NULL;
        return AT_ERROR_INVALID_RESPONSE;
    }

    return err;
}

int32_t at_send_command_sms(UART_Driver_t *uart_drv_handle, const char *command, const char *pdu, const char *responsePrefix,
                                 ATResponse **pp_outResponse)
{
    int32_t err;

    err = at_send_command_full(uart_drv_handle, command, SINGLELINE, responsePrefix, pdu, pp_outResponse,
                                        AT_COMM_DEFAULT_TIMEOUT);

    if (err == 0 && pp_outResponse != NULL
        && (*pp_outResponse)->success > 0
        && (*pp_outResponse)->p_intermediates == NULL
    ) {
        /* successful command must have an intermediate response */
        at_response_free(*pp_outResponse);
        *pp_outResponse = NULL;
        return AT_ERROR_INVALID_RESPONSE;
    }

    return err;
}

int32_t at_send_command_multiline(UART_Driver_t *uart_drv_handle, const char *command, const char *responsePrefix,
                                 ATResponse **pp_outResponse)
{
    int32_t err;

    err = at_send_command_full(uart_drv_handle, command, MULTILINE, responsePrefix, NULL, pp_outResponse,
                                    AT_COMM_DEFAULT_TIMEOUT);

    return err;
}
