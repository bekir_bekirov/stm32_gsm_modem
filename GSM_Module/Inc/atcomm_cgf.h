#ifndef AT_COMM_CFG_H
#define AT_COMM_CFG_H 1

#define USE_RTOS_AT_COM

#ifdef USE_RTOS_AT_COM
#define AT_COM_RTOS_MUTEX_TYPE					SemaphoreHandle_t
#define AT_COM_RTOS_MUTEX_CREATE(arg...)		xSemaphoreCreateMutex(arg)
#define AT_COM_RTOS_SEMPHR_CREATE(arg...)		xSemaphoreCreateBinary(arg)
#define AT_COM_RTOS_MUTEX_LOCK(arg...)			xSemaphoreTake(arg)
#define AT_COM_RTOS_MUTEX_UNLOCK(arg...)		xSemaphoreGive(arg)
#define AT_COM_RTOS_SEMPHR_TAKE(arg...)			xSemaphoreTake(arg)
#define AT_COM_RTOS_SEMPHR_GIVE(arg...)			xSemaphoreGive(arg)
#define AT_COM_RTOS_SEMPHR_GIVE_ISR(arg...)		xSemaphoreGiveFromISR(arg)
#define AT_COM_RTOS_SEMPHR_GET_CNT(arg...)      uxSemaphoreGetCount(arg)
#else
#define AT_COM_RTOS_MUTEX_TYPE					void*
#define AT_COM_RTOS_MUTEX_CREATE(arg...)
#define AT_COM_RTOS_SEMPHR_CREATE(arg...)
#define AT_COM_RTOS_MUTEX_LOCK(arg...)
#define AT_COM_RTOS_MUTEX_UNLOCK(arg...)
#define AT_COM_RTOS_SEMPHR_TAKE(arg...)
#define AT_COM_RTOS_SEMPHR_GIVE(arg...)
#define AT_COM_RTOS_SEMPHR_GIVE_ISR(arg...)
#endif

#define AT_COMM_DEFAULT_TIMEOUT             5000
#define MAX_AT_RESPONSE (128ul)

#endif
