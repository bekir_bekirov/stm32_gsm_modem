#ifndef SIM900_BSP_H
#define SIM900_BSP_H

void PowerOn_Sim900(void);
void PowerOff_Sim900(void);
void Init_Sim900(void);

#endif