#ifndef AT_COMM_H
#define AT_COMM_H 1

#include <stdint.h>
#include "stm32f4xx.h"
#include "uart.h"
#include "utils_misc.h"
#include "utarray.h"
#include "utlist.h"
#include "utringbuffer.h"


#define AT_ERROR_GENERIC          (-1)
#define AT_ERROR_COMMAND_PENDING  (-2)
#define AT_ERROR_CHANNEL_CLOSED   (-3)
#define AT_ERROR_TIMEOUT          (-4)
#define AT_ERROR_INVALID_THREAD   (-5) /* AT commands may not be issued from
                                          reader thread (or unsolicited response
                                          callback */
#define AT_ERROR_INVALID_RESPONSE (-6) /* eg an at_send_command_singleline that
                                          did not get back an intermediate
                                          response */

typedef enum {
    NO_RESULT,   /* no intermediate response expected */
    NUMERIC,     /* a single intermediate response starting with a 0-9 */
    SINGLELINE,  /* a single intermediate response starting with a prefix */
    MULTILINE    /* multiple line intermediate response
                    starting with a prefix */
} ATCommandType;

/** a singly-lined list of intermediate responses */
typedef struct ATLine  {
    struct ATLine *p_next;
    char *line;
} ATLine;

/** Free this with at_response_free() */
typedef struct {
    int success;              /* true if final response indicates
                                    success (eg "OK") */
    char *finalResponse;      /* eg OK, ERROR */
    ATLine  *p_intermediates; /* any intermediate responses */
} ATResponse;

/* for input buffering */

void InitATChannel(void);
void DeInitATChannel(void);
void AT_RX_IQR_Handler(UART_HandleTypeDef *huart);
int32_t at_send_command_full(UART_Driver_t *uart_drv_handle, const char *command, ATCommandType type,
                    const char *responsePrefix, const char *smspdu, ATResponse **pp_outResponse, TickType_t timeout);
int32_t at_send_command(UART_Driver_t *uart_drv_handle, const char *command, ATResponse **pp_outResponse);
int32_t at_send_command_singleline(UART_Driver_t *uart_drv_handle, const char *command, const char *responsePrefix,
                                    ATResponse **pp_outResponse);
int32_t at_send_command_numeric(UART_Driver_t *uart_drv_handle, const char *command, ATResponse **pp_outResponse);
int32_t at_send_command_sms(UART_Driver_t *uart_drv_handle, const char *command, const char *pdu, const char *responsePrefix,
                                 ATResponse **pp_outResponse);
int32_t at_send_command_multiline(UART_Driver_t *uart_drv_handle, const char *command, const char *responsePrefix,
                                 ATResponse **pp_outResponse);
const char *readline();
void *readerLoop(void);

typedef void (*ATUnsolHandler)(const char *s, const char *sms_pdu);

#endif
