include(${PROJECT_SOURCE_DIR}/cmake/base.cmake)

LibBuild(gsm_module_lib Inc ../utils/ut)

# target_compile_definitions(umm_malloc_lib PUBLIC
# )

target_link_libraries(gsm_module_lib PUBLIC
${TARGET}::stm32f429_cmsis_lib
${TARGET}::hal_stm32_lib
${TARGET}::dbg_system_lib
${TARGET}::utils_container_of_lib
${TARGET}::utils_misc_lib
${TARGET}::freertos_stm32_lib
)
