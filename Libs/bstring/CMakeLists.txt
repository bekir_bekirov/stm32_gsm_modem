include(${PROJECT_SOURCE_DIR}/cmake/base.cmake)

LibBuild(bstring_lib Inc)

target_compile_definitions(bstring_lib PUBLIC
BSTRLIB_CANNOT_USE_STL
BSTRLIB_CANNOT_USE_IOSTREAM
BSTRLIB_DOESNT_THROW_EXCEPTIONS
)

target_link_libraries(bstring_lib PUBLIC
${TARGET}::dbg_system_lib
${TARGET}::utils_container_of_lib
${TARGET}::utils_misc_lib
)
