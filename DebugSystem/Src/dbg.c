/*
 * uart_dbg.c
 *
 *  Created on: 6 дек. 2019 г.
 *      Author: bekir
 */

#include "dbg.h"
#include "uart.h"


DBG_System_t *Get_DBG_System_Context(void)
{
	static DBG_System_t dbg_system_ctx;

	return &dbg_system_ctx;
}

void Init_DBG_System(const UART_Driver_t *uart_handle, void *dbg_system_tx_cb)
{
	Get_DBG_System_Context()->uart_handle = uart_handle;
	Get_DBG_System_Context()->mutex = DBG_SYSTEM_RTOS_MUTEX_CREATE();
	Get_DBG_System_Context()->Send_Buf_DBG_System_cb = dbg_system_tx_cb;
    atomic_init(&Get_DBG_System_Context()->system_time, 0);
}

void Inc_SysTime_DBG_System(void)
{
    atomic_fetch_add(&Get_DBG_System_Context()->system_time, 1);
}

uint32_t Get_SysTime_DBG_System(void)
{
    return atomic_load(&Get_DBG_System_Context()->system_time);
}

void dbg_printf(const char *__msg, ...)
{
    static char buffer[LOGGER_MAXLEN] = {0};
    static _Atomic uint32_t interrupt_context;

    atomic_store(&interrupt_context, __get_IPSR());

    if (atomic_load(&interrupt_context) == 0) {
    	DBG_SYSTEM_RTOS_MUTEX_LOCK(Get_DBG_System_Context()->mutex, portMAX_DELAY);

		va_list ap;
		va_start (ap, __msg);
		vsprintf (buffer, __msg, ap);
		va_end (ap);

		if (Get_DBG_System_Context()->Send_Buf_DBG_System_cb) {
			Get_DBG_System_Context()->Send_Buf_DBG_System_cb(Get_DBG_System_Context()->uart_handle,
										buffer, strlen(buffer) + 1, portMAX_DELAY);
		}
		DBG_SYSTEM_RTOS_MUTEX_UNLOCK(Get_DBG_System_Context()->mutex);
    }
}
