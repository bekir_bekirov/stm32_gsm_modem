/*
 * debug_settings.h
 *
 *  Created on: 2 авг. 2020 г.
 *      Author: bekir
 */

#ifndef DBG_SETTINGS_INC_DBG_H_
#define DBG_SETTINGS_INC_DBG_H_

#define LOGGER_MAXLEN   500
#define PROJECT_DEBUG_COLOR
#define USE_RTOS_DBG_SYSTEM

#define GLOBAL_DEBUG_LEVEL 4

#endif
