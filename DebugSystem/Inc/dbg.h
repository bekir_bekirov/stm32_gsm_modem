/*
 * dbg.h
 *
 *  Created on: 6 дек. 2019 г.
 *      Author: bekir
 */

#ifndef DBG_INC_DBG_H_
#define DBG_INC_DBG_H_

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <stdatomic.h>

#include "debug_settings.h"
#include "uart.h"


#ifndef GLOBAL_DEBUG_LEVEL
#define GLOBAL_DEBUG_LEVEL 4
#endif

#ifndef LOGGER_MAXLEN
#define LOGGER_MAXLEN 500
#endif

#ifndef PROJECT_DEBUG_COLOR
#define PROJECT_DEBUG_COLOR
#endif

#ifdef USE_RTOS_DBG_SYSTEM
#define DBG_SYSTEM_RTOS_MUTEX_TYPE						SemaphoreHandle_t
#define DBG_SYSTEM_RTOS_MUTEX_CREATE(arg...)			xSemaphoreCreateMutex(arg)
#define DBG_SYSTEM_RTOS_MUTEX_LOCK(arg...)				xSemaphoreTake(arg)
#define DBG_SYSTEM_RTOS_MUTEX_UNLOCK(arg...)			xSemaphoreGive(arg)
#else
#define DBG_SYSTEM_RTOS_MUTEX_TYPE						void*
#define DBG_SYSTEM_RTOS_MUTEX_CREATE(arg...)
#define DBG_SYSTEM_RTOS_MUTEX_LOCK(arg...)
#define DBG_SYSTEM_RTOS_MUTEX_UNLOCK(arg...)
#endif

#define __FILENAME__ ((__builtin_strrchr(__FILE__, '/') ? __builtin_strrchr(__FILE__, '/') + 1 : __FILE__))

typedef struct DBG_system
{
	const UART_Driver_t *uart_handle;
	DBG_SYSTEM_RTOS_MUTEX_TYPE mutex;
    _Atomic uint32_t system_time;
	void (*Send_Buf_DBG_System_cb)(UART_Driver_t *uart_handle, const uint8_t *buf_tx, uint32_t cnt_tx, uint32_t timeout);
}DBG_System_t;

void dbg_printf(const char *__msg, ...);
DBG_System_t *Get_DBG_System_Context(void);
void Init_DBG_System(const UART_Driver_t *uart_handle, void *dbg_system_tx_cb);
void Inc_SysTime_DBG_System(void);
uint32_t Get_SysTime_DBG_System(void);

#ifdef PROJECT_DEBUG_COLOR

#define COLOR_ON_INFO               "\x1b[32m"
#define COLOR_ON_DEBUG              "\x1b[36m"
#define COLOR_ON_WARN	            "\x1b[33m"
#define COLOR_ON_ERROR              "\x1b[31m"
#define COLOR_OFF                   "\x1b[0m"

#else

#define COLOR_ON_INFO               ""
#define COLOR_ON_DEBUG              ""
#define COLOR_ON_WARN	            ""
#define COLOR_ON_ERROR              ""
#define COLOR_OFF                   ""

#endif

#if (GLOBAL_DEBUG_LEVEL > 0)
#define LOGV_ERROR(tag, x, arg...) dbg_printf(COLOR_ON_ERROR"%lu "#tag" - ""ERROR: %s %i ""\x1b[0m"x,	\
									Get_SysTime_DBG_System(), __FILENAME__, __LINE__, ##arg)
#define LOG_ERROR(tag, x, arg...) dbg_printf(COLOR_ON_ERROR"%lu "#tag" - ""\x1b[0m"x, Get_SysTime_DBG_System(), ##arg)
#else
#define LOG_ERROR(...)
#define LOGV_ERROR(...)
#endif

#if (GLOBAL_DEBUG_LEVEL > 1)
#define LOGV_WARN(tag, x, arg...) dbg_printf(COLOR_ON_WARN"%lu "#tag" - ""WARN: %s %i ""\x1b[0m"x,		\
									Get_SysTime_DBG_System(), __FILENAME__, __LINE__, ##arg)
#define LOG_WARN(tag, x, arg...) dbg_printf(COLOR_ON_WARN"%lu "#tag" - ""\x1b[0m"x, Get_SysTime_DBG_System(), ##arg)
#else
#define LOG_WARN(...)
#define LOGV_WARN(...)
#endif

#if (GLOBAL_DEBUG_LEVEL > 2)
#define LOGV_INFO(tag, x, arg...) dbg_printf(COLOR_ON_INFO"%lu "#tag" - ""INFO: %s %i ""\x1b[0m"x,		\
									Get_SysTime_DBG_System(), __FILENAME__, __LINE__, ##arg)
#define LOG_INFO(tag, x, arg...) dbg_printf(COLOR_ON_INFO"%lu "#tag" - ""\x1b[0m"x, Get_SysTime_DBG_System(), ##arg)
#else
#define LOG_INFO(...)
#define LOGV_INFO(...)
#endif

#if (GLOBAL_DEBUG_LEVEL > 3)
#define LOGV_DBG(tag, x, arg...) dbg_printf(COLOR_ON_DEBUG"%lu "#tag" - ""DBG: %s %i ""\x1b[0m"x,			\
									Get_SysTime_DBG_System(), __FILENAME__, __LINE__, ##arg)
#define LOG_DBG(tag, x, arg...) dbg_printf(COLOR_ON_DEBUG"%lu "#tag" - ""\x1b[0m"x, Get_SysTime_DBG_System(), ##arg)
#else
#define LOG_DBG(...)
#define LOGV_DBG(...)
#endif

#endif /* DBG_INC_DBG_H_ */
