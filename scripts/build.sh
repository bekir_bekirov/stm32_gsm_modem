#!/bin/bash

set -o errexit
set -o nounset

USAGE="Usage: $(basename $0) [-v | --verbose] [ reset | clean | debug | release | install]"

CMAKE=cmake
BUILD=./build
TYPE=
BUILD_DIR=$BUILD/debug
CLEAN=
RESET=
VERBOSE=
INSTALL=

CPU=$(nproc --all)

for arg; do
  case "$arg" in
    --help|-h)    echo $USAGE; exit 0;;
    -v|--verbose) VERBOSE='VERBOSE=1' ;;
    debug)        TYPE=Debug; BUILD_DIR=$BUILD/debug ;;
    release)      TYPE=Release; BUILD_DIR=$BUILD/release ;;
    clean)        CLEAN=1 ;;
    reset)        RESET=1 ;;
    install)      INSTALL=1 ;;   
   *)             echo -e "unknown option $arg\n$USAGE" >&2; exit 1 ;;
  esac
done

touch CMakeLists.txt

[[ -n $RESET && -d $BUILD_DIR ]] && rm -rf $BUILD_DIR
$CMAKE -S . -B $BUILD_DIR --warn-uninitialized \
-DCMAKE_BUILD_TYPE=$TYPE
[[ -n $CLEAN ]] && $CMAKE --build $BUILD_DIR --target clean
[[ -n $INSTALL ]] && $CMAKE --build $BUILD_DIR --target install
$CMAKE --build $BUILD_DIR --parallel $CPU -- $VERBOSE
