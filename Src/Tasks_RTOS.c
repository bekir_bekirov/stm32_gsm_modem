#define DEBUG_LEVEL 4
#define DEBUG_VERBOSE
#include "Tasks_RTOS_dbg.h"

#include "Tasks_RTOS.h"
#include "main.h"
#include "bstrlib.h"
#include "bstraux.h"
#include "stm32f4xx.h"
#include "atcomm.h"
#include "RIL.h"
#include "sim900_bsp.h"


extern UART_Driver_t uart_instance_gsm;
SemaphoreHandle_t task_mtx;

void TestTask1(void *pvParameters)
{
	ATResponse *response = NULL;

	xSemaphoreTake(task_mtx, portMAX_DELAY);
	at_send_command_full(&uart_instance_gsm, "ATE0\r", NO_RESULT, NULL, NULL, &response, 1000);
	// at_send_command_full(&uart_instance_gsm, "AT+CFUN=1,0\r", NO_RESULT, NULL, NULL, &response, 3000);
	xSemaphoreGive(task_mtx);

	for(;;) {
		LOGI(tasks_rtos, "TeskTask1 is running!\r\n");
		xSemaphoreTake(task_mtx, portMAX_DELAY);
		// at_send_command_full(&uart_instance_gsm, "AT\r", NO_RESULT, NULL, NULL, &response, 1000);
		at_send_command(&uart_instance_gsm, "AT\r", &response);
		at_response_free(response);

		requestSignalStrength(&uart_instance_gsm, NULL, 0);
		xSemaphoreGive(task_mtx);
		vTaskDelay(200);
	}
}

void TestTask2(void *pvParameters)
{
	ATResponse *response = NULL;

//	at_send_command_full(&uart_instance_gsm, "ATE0\r", NO_RESULT, NULL, NULL, &response, 1000);
	for(;;) {
		LOGI(tasks_rtos, "TeskTask2 is running!\r\n");
		xSemaphoreTake(task_mtx, portMAX_DELAY);
		at_send_command(&uart_instance_gsm, "AT\r", &response);
		at_response_free(response);
		xSemaphoreGive(task_mtx);

		
		vTaskDelay(100);
	}
}

void AtCommTask(void *pvParameters)
{
	readerLoop();
	LOGE(tasks_rtos, "AtCommTask is exit!\r\n");
	for(;;) {
		BSP_Led3_Toggle();
		vTaskDelay(100);
	}
}

void vApplicationStackOverflowHook(TaskHandle_t xTask, char * pcTaskName)
{
	return;
}

void vApplicationTickHook()
{

}
