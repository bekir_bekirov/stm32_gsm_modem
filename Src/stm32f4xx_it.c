/**
  ******************************************************************************
  * @file    stm32f4xx_it.c
  * @author  Ac6
  * @version V1.0
  * @date    02-Feb-2015
  * @brief   Default Interrupt Service Routines.
  ******************************************************************************
*/

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"
#include "stm32f4xx.h"
#ifdef USE_RTOS_SYSTICK
#include <cmsis_os.h>
#endif
#include "stm32f4xx_it.h"
#include "uart.h"
#include "atcomm.h"

extern TIM_HandleTypeDef htim6;
extern UART_Driver_t uart_instance;
extern UART_Driver_t uart_instance_gsm;

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

/******************************************************************************/
/*            	  	    Processor Exceptions Handlers                         */
/******************************************************************************/

/**
  * @brief  This function handles SysTick Handler, but only if no RTOS defines it.
  * @param  None
  * @retval None
  */
// void SysTick_Handler(void)
// {
// //	HAL_IncTick();
// 	HAL_SYSTICK_IRQHandler();

// //	 lv_tick_inc(1);

// #ifdef USE_RTOS_SYSTICK
// 	osSystickHandler();
// #endif
// }

void TIM6_DAC_IRQHandler(void)
{
	HAL_TIM_IRQHandler(&htim6);
}

void USART1_IRQHandler(void)
{
  HAL_UART_IRQHandler(&uart_instance.uart_instance);
}

void USART3_IRQHandler(void)
{
	uint32_t isrflags   = READ_REG(uart_instance_gsm.uart_instance.Instance->SR);

  if ((isrflags & USART_SR_RXNE) != RESET) {
	AT_RX_IQR_Handler(&uart_instance_gsm.uart_instance);
  __HAL_UART_CLEAR_FLAG(&uart_instance_gsm.uart_instance, UART_FLAG_RXNE);
  __HAL_UART_CLEAR_PEFLAG(&uart_instance_gsm.uart_instance);
  __HAL_UART_CLEAR_NEFLAG(&uart_instance_gsm.uart_instance);
  __HAL_UART_CLEAR_OREFLAG(&uart_instance_gsm.uart_instance);

  }
  else {
    if((isrflags & USART_SR_TC) != RESET)
    HAL_UART_IRQHandler(&uart_instance_gsm.uart_instance);
  }
}
