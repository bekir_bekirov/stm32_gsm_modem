/**
  ******************************************************************************
  * @file    main.c
  * @author  Bekir
  * @version V1.0
  * @date    1-02-2021
  * @brief   Default main function.
  ******************************************************************************
*/

#include "stm32f4xx.h"

#include <stdatomic.h>
#include "uart.h"
#include "dbg.h"
#include "Init_System_BSP.h"
#include "Tasks_RTOS.h"

#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "semphr.h"

#include "dbg.h"
#include "bstrlib.h"
#include "bstraux.h"
#include "umm_malloc_cfg.h"
#include "umm_malloc.h"
#include "utlist.h"
#include "utringbuffer.h"
#include "atcomm.h"
#include "sim900_bsp.h"

#define DEBUG_LEVEL 4
#define DEBUG_VERBOSE
#include "main_dbg.h"

#include "main.h"


UART_Driver_t uart_instance;
UART_Driver_t uart_instance_gsm;
extern SemaphoreHandle_t task_mtx;

static void Send_Buf_DBG_System(UART_Driver_t *uart_handle, const uint8_t *buf_tx, uint32_t cnt_tx, uint32_t timeout);

int main(void)
{
	HAL_Init();

	/* Configure the system clock to 180 MHz */
	SystemClock_Config();
	BSP_Leds_Init();

	/*Start up indication*/
	uint8_t i;
	for (i = 0; i < 8; i++) {
		HAL_Delay(50);
	}

	xTaskCreate(InitTask, "Init_Task", 2000, (void*) NULL, 3, NULL);
	vTaskStartScheduler();

	while (1)
	{
	}
}

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  if (htim->Instance == TIM6) {
    HAL_IncTick();
    Inc_SysTime_DBG_System();
  }
}

void Send_Buf_DBG_System(UART_Driver_t *uart_handle, const uint8_t *buf_tx, uint32_t cnt_tx, uint32_t timeout)
{
	UART_Tx(uart_handle, buf_tx, cnt_tx, portMAX_DELAY);
}

void InitTask(void *pvParameters)
{
	for(;;) {

		Init_Sim900();
		PowerOn_Sim900();
		vTaskDelay(5000);

		task_mtx = xSemaphoreCreateMutex();

		InitATChannel();
		Init_UART(&uart_instance, USART1, 115200, NULL);
		Init_UART(&uart_instance_gsm, USART3, 57600, NULL);
		Init_DBG_System(&uart_instance, Send_Buf_DBG_System);

		// LOGI(tasks_rtos, "GSMTask is running!, float = %f\r\n", 1.234f);

		xTaskCreate(AtCommTask, "AtCommTask", 2000, (void*) NULL, 2, NULL);
		xTaskCreate(TestTask1, "Test_Task1", 2000, (void*) NULL, 1, NULL);
		xTaskCreate(TestTask2, "Test_Task2", 2000, (void*) NULL, 1, NULL);

		vTaskDelete(NULL);
	}
}
