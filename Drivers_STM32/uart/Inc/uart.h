/*
 * uart.c
 *
 *  Created on: 1 мар. 2021 г.
 *      Author: bekir
 */

#ifndef DRIVERS_STM32_UART_INC_UART_H_
#define DRIVERS_STM32_UART_INC_UART_H_

#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include "stm32f4xx.h"

#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "semphr.h"

#include "container_of.h"
#include "uart_cfg.h"


typedef struct UART_Driver
{
	UART_HandleTypeDef uart_instance;
	uint32_t speed;
#ifdef USE_RTOS_UART
	UART_RTOS_MUTEX_TYPE mutex;
	UART_RTOS_MUTEX_TYPE mutex_rx;
	UART_RTOS_MUTEX_TYPE semphr;
#endif
	void (*UART_Ready_cb)(const struct UART_Driver *uart_handle);
}UART_Driver_t;

void Init_UART(UART_Driver_t *uart_handle_drv, USART_TypeDef *huart_instance, uint32_t speed, void *uart_ready_cb);
int32_t UART_Tx(UART_Driver_t *uart_handle_drv, const uint8_t *buf_tx, uint32_t cnt_tx, uint32_t timeout);
int8_t UART_Rx(UART_Driver_t *uart_handle_drv, const uint8_t *buf_rx, uint32_t cnt_rx, uint32_t timeout);
uint8_t UART_Rx_Get_Byte(UART_Driver_t *uart_handle_drv);

#endif /* DRIVERS_STM32_UART_INC_UART_H_ */
