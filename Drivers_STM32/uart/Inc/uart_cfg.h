/*
 * uart_cfg.h
 *
 *  Created on: 1 мар. 2021 г.
 *      Author: bekir
 */

#ifndef DRIVERS_STM32_UART_INC_UART_CFG_H_
#define DRIVERS_STM32_UART_INC_UART_CFG_H_

#define USE_RTOS_UART

#ifdef USE_RTOS_UART
#define UART_RTOS_MUTEX_TYPE					SemaphoreHandle_t
#define UART_RTOS_MUTEX_CREATE(arg...)			xSemaphoreCreateMutex(arg)
#define UART_RTOS_SEMPHR_CREATE(arg...)			xSemaphoreCreateBinary(arg)
#define UART_RTOS_MUTEX_LOCK(arg...)			xSemaphoreTake(arg)
#define UART_RTOS_MUTEX_UNLOCK(arg...)			xSemaphoreGive(arg)
#define UART_RTOS_SEMPHR_TAKE(arg...)			xSemaphoreTake(arg)
#define UART_RTOS_SEMPHR_GIVE(arg...)			xSemaphoreGive(arg)
#define UART_RTOS_SEMPHR_GIVE_ISR(arg...)		xSemaphoreGiveFromISR(arg)
#else
#define UART_RTOS_MUTEX_TYPE					void*
#define UART_RTOS_MUTEX_CREATE(arg...)
#define UART_RTOS_SEMPHR_CREATE(arg...)
#define UART_RTOS_MUTEX_LOCK(arg...)
#define UART_RTOS_MUTEX_UNLOCK(arg...)
#define UART_RTOS_SEMPHR_TAKE(arg...)
#define UART_RTOS_SEMPHR_GIVE(arg...)
#define UART_RTOS_SEMPHR_GIVE_ISR(arg...)
#endif

#endif /* DRIVERS_STM32_UART_INC_UART_CFG_H_ */
