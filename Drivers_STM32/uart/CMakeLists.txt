include(${PROJECT_SOURCE_DIR}/cmake/base.cmake)

LibBuild(uart_srm32_driver_lib Inc)

target_compile_definitions(uart_srm32_driver_lib PUBLIC USE_HAL_DRIVER STM32F4)

target_link_libraries(uart_srm32_driver_lib PUBLIC
${TARGET}::stm32f429_cmsis_lib
${TARGET}::hal_stm32_lib
${TARGET}::freertos_stm32_lib
${TARGET}::utils_container_of_lib
${TARGET}::utils_misc_lib
)
