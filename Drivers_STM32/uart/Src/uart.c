/*
 * uart.c
 *
 *  Created on: 1 мар. 2021 г.
 *      Author: bekir
 */

#include "uart.h"

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  None
  * @retval None
  */
void Error_Handler(void)
{
  while(1)
  {
  }
}

static DMA_HandleTypeDef hdma_uart_tx;
static DMA_HandleTypeDef hdma_uart_tx_gsm;

static void USART_DMA_Init(void);

void Init_UART(UART_Driver_t *uart_handle_drv, USART_TypeDef *huart_instance, uint32_t speed, void *uart_ready_cb)
{
#ifdef USE_RTOS_UART
	uart_handle_drv->mutex = UART_RTOS_MUTEX_CREATE();
	uart_handle_drv->mutex_rx = UART_RTOS_MUTEX_CREATE();
	uart_handle_drv->semphr = UART_RTOS_SEMPHR_CREATE();
#endif

	if (uart_ready_cb) uart_handle_drv->UART_Ready_cb = uart_ready_cb;
	USART_DMA_Init();
	uart_handle_drv->uart_instance.Instance = huart_instance;
	uart_handle_drv->uart_instance.Init.BaudRate = speed;
	uart_handle_drv->uart_instance.Init.WordLength = UART_WORDLENGTH_8B;
	uart_handle_drv->uart_instance.Init.StopBits = UART_STOPBITS_1;
	uart_handle_drv->uart_instance.Init.Parity = UART_PARITY_NONE;
	uart_handle_drv->uart_instance.Init.Mode = UART_MODE_TX_RX;
	uart_handle_drv->uart_instance.Init.HwFlowCtl = UART_HWCONTROL_NONE;
	uart_handle_drv->uart_instance.Init.OverSampling = UART_OVERSAMPLING_16;
	if (HAL_UART_Init(&uart_handle_drv->uart_instance) != HAL_OK)
	{
	Error_Handler();
	}
	__HAL_UART_ENABLE_IT(&uart_handle_drv->uart_instance, UART_IT_RXNE);
}

int32_t UART_Tx(UART_Driver_t *uart_handle_drv, const uint8_t *buf_tx, uint32_t cnt_tx, uint32_t timeout)
{
	int32_t res = -1;

#ifdef USE_RTOS_UART
	if (UART_RTOS_MUTEX_LOCK(uart_handle_drv->mutex, timeout) == pdTRUE) {
		HAL_UART_Transmit_DMA(&uart_handle_drv->uart_instance, buf_tx, cnt_tx);
		res = (UART_RTOS_SEMPHR_TAKE(uart_handle_drv->semphr, timeout) == pdTRUE) ? 0 : -1;
		UART_RTOS_MUTEX_UNLOCK(uart_handle_drv->mutex);
	} else return -1;
#else
	if (HAL_UART_Transmit(&uart_handle_drv->uart_instance, buf_tx, cnt_tx, timeout) == HAL_OK) {
		if (uart_handle_drv->UART_Ready_cb) uart_handle_drv->UART_Ready_cb(uart_handle_drv);
		res = 0;
    }
#endif

	return res;
}

int8_t UART_Rx(UART_Driver_t *uart_handle_drv, const uint8_t *buf_rx, uint32_t cnt_rx, uint32_t timeout)
{
	#ifdef USE_RTOS_UART
	UART_RTOS_MUTEX_LOCK(uart_handle_drv->mutex_rx, timeout);
	if (HAL_UART_Receive_IT(&uart_handle_drv->uart_instance, buf_rx, cnt_rx) == HAL_OK) {
		if (uart_handle_drv->UART_Ready_cb) uart_handle_drv->UART_Ready_cb(uart_handle_drv);
		UART_RTOS_MUTEX_UNLOCK(uart_handle_drv->mutex_rx);
		return 0;
	} else {
		UART_RTOS_MUTEX_UNLOCK(uart_handle_drv->mutex_rx);
		return -1;
	}
	UART_RTOS_MUTEX_UNLOCK(uart_handle_drv->mutex_rx);
#else
	if (HAL_UART_Receive_IT(&uart_handle_drv->uart_instance, buf_rx, cnt_rx) == HAL_OK) {
		if (uart_handle_drv->UART_Ready_cb) uart_handle_drv->UART_Ready_cb(uart_handle_drv);
		return 0;
    } else {
		return -1;
	}
#endif
}

uint8_t UART_Rx_Get_Byte(UART_Driver_t *uart_handle_drv)
{
	return (uint8_t)(uart_handle_drv->uart_instance.Instance->DR & (uint16_t)0x00FF);
}

/**
  * Enable DMA controller clock
  */
static void USART_DMA_Init(void)
{
	/* DMA controller clock enable */
	__HAL_RCC_DMA2_CLK_ENABLE();
	__HAL_RCC_DMA1_CLK_ENABLE();

	/* DMA interrupt init */
	/* DMA2_Stream7_IRQn interrupt configuration */
	HAL_NVIC_SetPriority(DMA2_Stream7_IRQn, 13, 0);
	HAL_NVIC_EnableIRQ(DMA2_Stream7_IRQn);

	/* DMA interrupt init */
	/* DMA1_Stream3_IRQn interrupt configuration */
	HAL_NVIC_SetPriority(DMA1_Stream3_IRQn, 13, 0);
	HAL_NVIC_EnableIRQ(DMA1_Stream3_IRQn);
}

/**
* @brief UART MSP Initialization
* This function configures the hardware resources used in this example
* @param huart: UART handle pointer
* @retval None
*/
void HAL_UART_MspInit(UART_HandleTypeDef* huart)
{
	GPIO_InitTypeDef GPIO_InitStruct = {0};
	if(huart->Instance==USART1)
	{
		/* Peripheral clock enable */
		__HAL_RCC_USART1_CLK_ENABLE();

		__HAL_RCC_GPIOA_CLK_ENABLE();
		/**USART1 GPIO Configuration
		PA9     ------> USART1_TX
		PA10     ------> USART1_RX
		*/
		GPIO_InitStruct.Pin = GPIO_PIN_9|GPIO_PIN_10;
		GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
		GPIO_InitStruct.Pull = GPIO_NOPULL;
		GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
		GPIO_InitStruct.Alternate = GPIO_AF7_USART1;
		HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

		/* USART1 DMA Init */
		/* USART1_TX Init */
		hdma_uart_tx.Instance = DMA2_Stream7;
		hdma_uart_tx.Init.Channel = DMA_CHANNEL_4;
		hdma_uart_tx.Init.Direction = DMA_MEMORY_TO_PERIPH;
		hdma_uart_tx.Init.PeriphInc = DMA_PINC_DISABLE;
		hdma_uart_tx.Init.MemInc = DMA_MINC_ENABLE;
		hdma_uart_tx.Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
		hdma_uart_tx.Init.MemDataAlignment = DMA_MDATAALIGN_BYTE;
		hdma_uart_tx.Init.Mode = DMA_NORMAL;
		hdma_uart_tx.Init.Priority = DMA_PRIORITY_LOW;
		hdma_uart_tx.Init.FIFOMode = DMA_FIFOMODE_DISABLE;
		if (HAL_DMA_Init(&hdma_uart_tx) != HAL_OK)
		{
		Error_Handler();
		}

		__HAL_LINKDMA(huart, hdmatx, hdma_uart_tx);

		#ifdef USE_RTOS_UART
		/* USART1 interrupt Init */
		HAL_NVIC_SetPriority(USART1_IRQn, 13, 0);
		HAL_NVIC_EnableIRQ(USART1_IRQn);
		#endif
	}

	if(huart->Instance==USART3) {
		/* Peripheral clock enable */
		__HAL_RCC_USART3_CLK_ENABLE();

		__HAL_RCC_GPIOD_CLK_ENABLE();
		/**USART3 GPIO Configuration    
		PD8     ------> USART3_TX
		PD9     ------> USART3_RX 
		*/
		GPIO_InitStruct.Pin = GPIO_PIN_8|GPIO_PIN_9;
		GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
		GPIO_InitStruct.Pull = GPIO_PULLUP;
		GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
		GPIO_InitStruct.Alternate = GPIO_AF7_USART3;
		HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

		/* USART3 DMA Init */
		/* USART3_TX Init */
		hdma_uart_tx_gsm.Instance = DMA1_Stream3;
		hdma_uart_tx_gsm.Init.Channel = DMA_CHANNEL_4;
		hdma_uart_tx_gsm.Init.Direction = DMA_MEMORY_TO_PERIPH;
		hdma_uart_tx_gsm.Init.PeriphInc = DMA_PINC_DISABLE;
		hdma_uart_tx_gsm.Init.MemInc = DMA_MINC_ENABLE;
		hdma_uart_tx_gsm.Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
		hdma_uart_tx_gsm.Init.MemDataAlignment = DMA_MDATAALIGN_BYTE;
		hdma_uart_tx_gsm.Init.Mode = DMA_NORMAL;
		hdma_uart_tx_gsm.Init.Priority = DMA_PRIORITY_LOW;
		hdma_uart_tx_gsm.Init.FIFOMode = DMA_FIFOMODE_DISABLE;
		if (HAL_DMA_Init(&hdma_uart_tx_gsm) != HAL_OK)
		{
		Error_Handler();
		}

		__HAL_LINKDMA(huart, hdmatx, hdma_uart_tx_gsm);

		/* USART3 interrupt Init */
		HAL_NVIC_SetPriority(USART3_IRQn, 13, 0);
		HAL_NVIC_EnableIRQ(USART3_IRQn);
	}
}

/**
* @brief UART MSP De-Initialization
* This function freeze the hardware resources used in this example
* @param huart: UART handle pointer
* @retval None
*/
void HAL_UART_MspDeInit(UART_HandleTypeDef* huart)
{
	if(huart->Instance==USART1)
	{
		/* Peripheral clock disable */
		__HAL_RCC_USART1_CLK_DISABLE();

		/**USART1 GPIO Configuration
		PA9     ------> USART1_TX
		PA10     ------> USART1_RX
		*/
		HAL_GPIO_DeInit(GPIOA, GPIO_PIN_9|GPIO_PIN_10);

		/* USART1 DMA DeInit */
		HAL_DMA_DeInit(huart->hdmatx);

		/* USART1 interrupt DeInit */
		HAL_NVIC_DisableIRQ(USART1_IRQn);
	}

	if (huart->Instance==USART3)
	{
		/* Peripheral clock disable */
		__HAL_RCC_USART3_CLK_DISABLE();

		/**USART3 GPIO Configuration    
		PD8     ------> USART3_TX
		PD9     ------> USART3_RX 
		*/
		HAL_GPIO_DeInit(GPIOD, GPIO_PIN_8|GPIO_PIN_9);

		/* USART3 DMA DeInit */
		HAL_DMA_DeInit(huart->hdmatx);

		/* USART3 interrupt DeInit */
		HAL_NVIC_DisableIRQ(USART3_IRQn);
	}
}

void DMA2_Stream7_IRQHandler(void)
{
  HAL_DMA_IRQHandler(&hdma_uart_tx);
}

void DMA1_Stream3_IRQHandler(void)
{
  HAL_DMA_IRQHandler(&hdma_uart_tx_gsm);
}

void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart)
{
	static signed long xHigherPriorityTaskWoken[2];

	UART_Driver_t *uart_ctx = container_of(huart, UART_Driver_t, uart_instance);
	if (huart->Instance==USART1) {
		if (uart_ctx->UART_Ready_cb) uart_ctx->UART_Ready_cb(uart_ctx);

		UART_RTOS_SEMPHR_GIVE_ISR(uart_ctx->semphr, &xHigherPriorityTaskWoken[0]);
		portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
	}

	if (huart->Instance==USART3) {
		if (uart_ctx->UART_Ready_cb) uart_ctx->UART_Ready_cb(uart_ctx);

		UART_RTOS_SEMPHR_GIVE_ISR(uart_ctx->semphr, &xHigherPriorityTaskWoken[1]);
		portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
	}
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
	UART_Driver_t *uart_ctx = container_of(huart, UART_Driver_t, uart_instance);
		if (huart->Instance==USART3) {
		if (uart_ctx->UART_Ready_cb) uart_ctx->UART_Ready_cb(uart_ctx);
		HAL_GPIO_TogglePin(GPIOG, GPIO_PIN_14);
	}
}
