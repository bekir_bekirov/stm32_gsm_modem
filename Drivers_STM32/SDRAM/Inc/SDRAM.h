/*
 * SDRAM.h
 *
 *  Created on: 27 февр. 2021 г.
 *      Author: bekir
 */

#ifndef DRIVERS_STM32_SDRAM_INC_SDRAM_H_
#define DRIVERS_STM32_SDRAM_INC_SDRAM_H_

#define REFRESH_COUNT       ((uint32_t)0x056A)   /* SDRAM refresh counter (90MHz SDRAM clock) */
#define SDRAM_BANK_ADDR     ((uint32_t)0xD0000000)
/* #define SDRAM_MEMORY_WIDTH            FMC_SDRAM_MEM_BUS_WIDTH_8 */
#define SDRAM_MEMORY_WIDTH            FMC_SDRAM_MEM_BUS_WIDTH_16

/* #define SDCLOCK_PERIOD                   FMC_SDRAM_CLOCK_PERIOD_2 */
#define SDCLOCK_PERIOD                FMC_SDRAM_CLOCK_PERIOD_3

#define SDRAM_TIMEOUT     ((uint32_t)0xFFFF)

#define SDRAM_MODEREG_BURST_LENGTH_1             ((uint16_t)0x0000)
#define SDRAM_MODEREG_BURST_LENGTH_2             ((uint16_t)0x0001)
#define SDRAM_MODEREG_BURST_LENGTH_4             ((uint16_t)0x0002)
#define SDRAM_MODEREG_BURST_LENGTH_8             ((uint16_t)0x0004)
#define SDRAM_MODEREG_BURST_TYPE_SEQUENTIAL      ((uint16_t)0x0000)
#define SDRAM_MODEREG_BURST_TYPE_INTERLEAVED     ((uint16_t)0x0008)
#define SDRAM_MODEREG_CAS_LATENCY_2              ((uint16_t)0x0020)
#define SDRAM_MODEREG_CAS_LATENCY_3              ((uint16_t)0x0030)
#define SDRAM_MODEREG_OPERATING_MODE_STANDARD    ((uint16_t)0x0000)
#define SDRAM_MODEREG_WRITEBURST_MODE_PROGRAMMED ((uint16_t)0x0000)
#define SDRAM_MODEREG_WRITEBURST_MODE_SINGLE     ((uint16_t)0x0200)

#define FB_SDRAM_ADDR_1     ((uint32_t)SDRAM_BANK_ADDR)
#define FB_SDRAM_ADDR_2		((uint32_t)(SDRAM_BANK_ADDR + 0x40000))
#define FB_SDRAM_ADDR_3		((uint32_t)(SDRAM_BANK_ADDR + 0x80000))

/*SD RAM*/
void SDRAM_Init(void);

#endif /* DRIVERS_STM32_SDRAM_INC_SDRAM_H_ */
